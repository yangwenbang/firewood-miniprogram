const app = getApp()

Page({

    /**
     * 页面的初始数据
     */
    data: {
        selected: null,
        orderTempPhoto: null,
        evaluationTempPhoto: null,
        orderCodeTempPhoto: null,
        checkCodeTempPhoto: null,
        orderCode: null,
        contact: null,
        userOpenID: null,
        shops: null
    },
    getPhoto: function(event) {
        var that = this;
        var phototype = event.currentTarget.dataset.phototype;
        wx.chooseImage({
            sizeType: ['original', 'compressed'],
            sourceType: ['album', 'camera'],
            count: 1,
            success: function(res) {
                if (phototype == 'order') {
                    that.setData({
                        orderTempPhoto: res.tempFilePaths[0]
                    })
                    wx.uploadFile({
                        url: 'https://www.firewood666.com/firewood/admin/uploadFile',
                        filePath: that.data.orderTempPhoto,
                        name: 'file',
                        formData: {},
                        method: 'POST',
                        header: {
                            "Content-Type": "multipart/form-data"
                        },
                        success(res) {
                            var result = JSON.parse(res.data);
                            that.data.orderTempPhoto = result.data;
                        }
                    })
                } else if (phototype == 'evaluation') {
                    that.setData({
                        evaluationTempPhoto: res.tempFilePaths[0]
                    })
                    wx.uploadFile({
                        url: 'https://www.firewood666.com/firewood/admin/uploadFile',
                        filePath: that.data.evaluationTempPhoto,
                        name: 'file',
                        formData: {},
                        method: 'POST',
                        header: {
                            "Content-Type": "multipart/form-data"
                        },
                        success(res) {
                            var result = JSON.parse(res.data);
                            that.data.evaluationTempPhoto = result.data;
                        }
                    })
                } else if (phototype == 'orderCode') {
                    that.setData({
                        orderCodeTempPhoto: res.tempFilePaths[0]
                    })
                    wx.uploadFile({
                        url: 'https://www.firewood666.com/firewood/admin/uploadFile',
                        filePath: that.data.orderCodeTempPhoto,
                        name: 'file',
                        formData: {},
                        method: 'POST',
                        header: {
                            "Content-Type": "multipart/form-data"
                        },
                        success(res) {
                            var result = JSON.parse(res.data);
                            that.data.orderCodeTempPhoto = result.data;
                        }
                    })
                } else if (phototype == 'checkCode') {
                    that.setData({
                        checkCodeTempPhoto: res.tempFilePaths[0]
                    })
                    wx.uploadFile({
                        url: 'https://www.firewood666.com/firewood/admin/uploadFile',
                        filePath: that.data.checkCodeTempPhoto,
                        name: 'file',
                        formData: {},
                        method: 'POST',
                        header: {
                            "Content-Type": "multipart/form-data"
                        },
                        success(res) {
                            var result = JSON.parse(res.data);
                            that.data.checkCodeTempPhoto = result.data;
                        }
                    })
                }
            }
        })
    },

    preview: function(event) {
        var that = this;
        var src = event.currentTarget.dataset.src;
        var imgList = [src]; //获取data-list
        //图片预览
        wx.previewImage({
            current: src, // 当前显示图片的http链接
            urls: imgList // 需要预览的图片http链接列表
        })
    },


    orderCodeInput: function(event) {
        this.setData({
            orderCode: event.detail.value
        })
    },

    contactInput: function(event) {
        this.setData({
            contact: event.detail.value
        })
    },

    change: function(e) {
        this.setData({
            selected: {...e.detail }
        });
        console.log(this.data.selected);
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function(options) {
        var that = this;
        that.setData({
            userOpenID: options.userOpenID,
            shops: app.globalData.shops
        })

    },

    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function() {},

    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function() {},

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function() {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload: function() {},

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function() {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function() {

    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage: function() {

    },

    submit: function(event) {
        var that = this;
        if (that.data.selected.id == '000' || !that.data.selected.id) {
            wx.showToast({
                title: '请选择门店',
                icon: 'none',
                image: '/images/fail.png',
                duration: 2000
            })
            return;
        }
        if (that.data.orderTempPhoto == null) {
            wx.showToast({
                title: '请上传订单截图',
                icon: 'none',
                image: '/images/fail.png',
                duration: 2000
            })
            return;
        }
        if (that.data.evaluationTempPhoto == null) {
            wx.showToast({
                title: '请上传评价截图',
                icon: 'none',
                image: '/images/fail.png',
                duration: 2000
            })
            return;
        }
        if (that.data.orderCodeTempPhoto == null) {
            wx.showToast({
                title: '请上传订单号截图',
                icon: 'none',
                image: '/images/fail.png',
                duration: 2000
            })
            return;
        }
        if (that.data.checkCodeTempPhoto == null) {
            wx.showToast({
                title: '请上传核销码截图',
                icon: 'none',
                image: '/images/fail.png',
                duration: 2000
            })
            return;
        }
        if (that.data.orderCode == null) {
            wx.showToast({
                title: '请输入订单号',
                icon: 'none',
                image: '/images/fail.png',
                duration: 2000
            })
            return;
        }
        if (that.data.contact == null) {
            wx.showToast({
                title: '请输入微信号或手机号',
                icon: 'none',
                image: '/images/fail.png',
                duration: 2000
            })
            return;
        }

        wx.showLoading({
            title: '正在上传',
        })

        wx.request({
            url: 'https://www.firewood666.com/firewood/app/insertCashBack',
            data: {
                "evaluationPicUrl": that.data.evaluationTempPhoto,
                "orderNo": that.data.orderCode,
                "orderNoPicUrl": that.data.orderCodeTempPhoto,
                "orderPicUrl": that.data.orderTempPhoto,
                "shopId": that.data.selected.id,
                "userOpenId": that.data.userOpenID,
                "verificationCodeUrl": that.data.checkCodeTempPhoto,
                "wechatCode": that.data.contact
            },
            header: {
                'content-type': 'application/json'
            },
            method: 'post',
            success(res) {
                wx.showToast({
                    title: '提交成功',
                })
                wx.navigateTo({
                    url: '../index/index',
                    success: function(res) {},
                    fail: function(res) {},
                    complete: function(res) {},
                })
            },
            fail() {
                wx.showToast({
                    title: '提交失败',
                })
            }
        })
    }
})