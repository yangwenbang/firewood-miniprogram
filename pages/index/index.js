const app = getApp()

Page({
    data: {
        sysInfo: null,
        height: app.globalData.menutop + app.globalData.menuButtonRect[1] + 3,
        user_id: null,
        user_OpenID: null
    },

    login: function(e) {
        var that = this;
        console.log(e.detail.userInfo);
        if (!e.detail.userInfo) {
            wx.showToast({
                title: '获取用户信息失败',
                icon: 'none',
                image: '',
                duration: 500,
                mask: true,
                success: function(res) {},
                fail: function(res) {},
                complete: function(res) {},
            })
            return
        }

        wx.login({
            success(res) {
                console.log(res.code);
                console.log(e)
                wx.request({
                    url: 'https://www.firewood666.com/firewood/app/loginByWechat',
                    data: {
                        code: res.code,
                        nickName: e.detail.userInfo.nickName,
                        avatarUrl: e.detail.userInfo.avatarUrl,
                        gender: e.detail.userInfo.gender,
                        language: e.detail.userInfo.language,
                        province: e.detail.userInfo.province,
                        country: e.detail.userInfo.country,
                        city: e.detail.userInfo.city
                    },
                    header: {
                        'content-type': 'application/json'
                    },
                    method: 'post',
                    success(res) {
                        that.data.user_OpenID = res.data.data;
                        app.globalData.islogin = true
                        wx.navigateTo({
                            url: '../uploadPhoto/uploadPhoto?userOpenID=' + that.data.user_OpenID,
                            success: function(res) {},
                            fail: function(res) {},
                            complete: function(res) {},
                        })
                    },
                    fail() {
                        wx.showToast({
                            title: '登录失败',
                        })
                    }
                })
            }
        })

    },

    onLoad: function() {
        var that = this;
        wx.getStorage({
            key: 'sysInfo',
            success: function(res) {
                that.setData({
                    sysInfo: res
                })
            },
        })
    }

})